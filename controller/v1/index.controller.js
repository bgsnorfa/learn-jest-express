const { success, error } = require("../../utils/response/json.utils");

exports.index = function (req, res) {
    return success(res, 200, "OK", {});
}

exports.addition = function (req, res) {
    let { num1, num2 } = req.body;

    if (!isNaN(num1) && !isNaN(num2)) {
        return success(res, 200, "", {result: num1 + num2});
    } else {
        return error(res, 400, "", {});
    }
}

exports.substraction = function (req, res) {
    let { num1, num2 } = req.body;
    if (!isNaN(num1) && !isNaN(num2)) {
        return success(res, 200, "", {
            result: num1 - num2
        });
    } else {
        return error(res, 400, "", {});
    }
}
