const express = require("express");
const { index, addition, substraction } =  require("../controller/v1/index.controller");

const router = express.Router();

router.get("/", index);
router.post("/addition", addition);
router.post("/substraction", substraction);

module.exports = router;