const base = require("../../../controller/v1/index.controller");
const mockRequest = (body = {}) => ({ body });
const mockResponse = () => {
    const res = {};
    res.json = jest.fn().mockReturnValue(res);
    res.status = jest.fn().mockReturnValue(res);
    return res;
}

describe('GET /', () => {
    test('res.json', done => {
        const req = mockRequest();
        const res = mockResponse();

        base.index(req, res);
        expect(res.status).toBeCalledWith(200);
        expect(res.json).toBeCalledWith({
            message: "OK",
            data: {}
        });
        done();
    })
})

describe('POST /addition', () => {
    let num1 = 5;
    let num2 = 10;

    test('res.json', done => {
        const req = {
            body: {
                num1,
                num2
            }
        };

        const res = mockResponse();

        base.addition(req, res);
        expect(res.status).toBeCalledWith(200);
        expect(res.json).toBeCalledWith({
            message: "",
            data: {
                result: num1 + num2
            }
        });
        done();
    })
})

describe('POST /substraction for 10 - 5', () => {
    let num1 = 10;
    let num2 = 5;

    test('res.json', done => {
        const req = {
            body: {
                num1,
                num2
            }
        };
        const res = mockResponse();

        base.substraction(req, res);
        expect(res.status).toBeCalledWith(200);
        expect(res.json).toBeCalledWith({
            message: "",
            data: {
                result: num1 - num2
            }
        });
        done();
    })
})

describe('POST /substraction for 5 - 10', () => {
    let num1 = 5;
    let num2 = 10;

    test('res.json', done => {
        const req = {
            body: {
                num1,
                num2
            }
        };
        const res = mockResponse();

        base.substraction(req, res);
        expect(res.status).toBeCalledWith(200);
        expect(res.json).toBeCalledWith({
            message: "",
            data: {
                result: num1 - num2
            }
        });
        done();
    })
})

describe('POST /substraction for a - b', () => {
    let num1 = "a";
    let num2 = "b";

    test('res.json', done => {
        const req = {
            body: {
                num1,
                num2
            }
        };
        const res = mockResponse();

        base.substraction(req, res);
        expect(res.status).toBeCalledWith(400);
        expect(res.json).toBeCalledWith({
            error: {
                message: "",
                data: {}
            }
        });
        done();
    })
})